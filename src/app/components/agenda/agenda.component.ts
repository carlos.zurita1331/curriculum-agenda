import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder,Validators } from '@angular/forms';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ApiService } from 'src/app/services/api.service';
import { AgregarComponent } from './agregar.component';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit {

  productForm !: FormGroup;

  displayedColumns: string[] = ['Nombre', 'Apellido', 'Celular', 'Fecha','Hora','Desc','acciones'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;


  constructor(private dialog:MatDialog, private api: ApiService) { }

  ngOnInit(): void {
    this.getAllDates();
  }

  openDialog() {
    this.dialog.open(AgregarComponent, {
      width:'30%'
    }).afterClosed().subscribe(val=>{
      if (val === 'save') {
        this.getAllDates();
      }
    });
  }

  getAllDates(){
    this.api.getProduct()
    .subscribe({
      next:(res)=>{
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

      },
      error: (err)=>{
        alert("Error mientras se realiza el servicio")
      }
    })
  }

  editDate(row:any){
    this.dialog.open(AgregarComponent,{
      width:'30%',
      data:row
    }).afterClosed().subscribe(val=>{
      if (val === 'update') {
        this.getAllDates();
      }
    })
  }

  deleteDate(id:number){
    this.api.deleteProduct(id)
    .subscribe({
      next:(res)=>{
        alert("date has delete successfully")
        this.getAllDates();
      },
      error:()=>{
        alert("Error while deleting date")
      }
    })
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
