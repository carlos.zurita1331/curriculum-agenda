import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog'

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {

    productForm !: FormGroup;
    actionbtn : string = "save"

  constructor(private formBuilder : FormBuilder, private api: ApiService,
    @Inject(MAT_DIALOG_DATA) public editData : any,
    private dialogRef : MatDialogRef<AgregarComponent>) { }

  ngOnInit(): void {
    this.productForm = this.formBuilder.group({
      Nombre:['',Validators.required],
      Apellido:['',Validators.required],
      Email:['',Validators.required],
      Celular:['',Validators.required],
      Fecha:['',Validators.required],
      Hora:['',Validators.required],
      Desc:['',Validators.required]
    })

    if (this.editData) {
      this.actionbtn = "Update"
        this.productForm.controls['Nombre'].setValue(this.editData.Nombre);
        this.productForm.controls['Apellido'].setValue(this.editData.Apellido);
        this.productForm.controls['Email'].setValue(this.editData.Email);
        this.productForm.controls['Celular'].setValue(this.editData.Celular);
        this.productForm.controls['Fecha'].setValue(this.editData.Fecha);
        this.productForm.controls['Hora'].setValue(this.editData.Hora);
        this.productForm.controls['Desc'].setValue(this.editData.Desc);

    }
    
  }

  addDate(){
    if (!this.editData) {

      if(this.productForm.valid){
        this.api.postProduct(this.productForm.value)
        .subscribe({
          next:(res)=>{
            alert("dato has added successully");
            this.productForm.reset();
            this.dialogRef.close('save');
          },
          error:()=>{
            alert("Error date has do not added")
          }
        })
      }
      
    }else{
      this.updateData()
    }
    
  }

  updateData(){
    this.api.putProduct(this.productForm.value,this.editData.id)
    .subscribe({
      next: (res)=>{
        alert("Date has update successfully")
        this.productForm.reset();
        this.dialogRef.close('update');
      },
      error:()=>{
        alert("Error while updating date record")
      }
    })
  }

}
